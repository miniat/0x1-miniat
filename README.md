The **MiniAT** is an open-source embedded microcontroller packaged as an easy to
use C library, designed for and with computer science students. Using any
C-friendly language and the **MiniAT** development toolchain, users prototype and
simulate interactive systems of their own design. The **MiniAT** interface is
extensible and flexible, so you can even use "off-the-shelf" peripherals or
just enjoy the existing systems provided by the project and its community.

# Build and Installation

Required software includes:

- C compiler, such as `gcc`, `clang`, etc
- CMake 3.16 or newer
- A build tool, such as `make`
- Flex, the fast lexical analyzer generator

Optional software incldes:

- NCurses development files, such as `ncurses` (for Linux/Mac) or `pdcurses`
(for Windows and MinGW) if you want to build and use the **MiniAT** Color
Console system.

On a Linux system like Ubuntu (or Windows Subsystem for Linux using Ubuntu),
the required software can be installed with the command...

	sudo apt install gcc cmake make flex libncurses-dev

To build the **MiniAT**, from a terminal, go to the root of your **MiniAT**
sources, say `~/miniat`, and run the following sequnce of commands (presuming
you're on a Linux or Mac... Windows commands are similar):

1. `mkdir build`
2. `cd build`
3. `cmake .. -DCMAKE_INSTALL_PREFIX=./out` (you can specify a different
installation path, but this will at least keep everything built within the
source tree until you decide to deploy elsewhere)
4. `make install` (this will build and move all targets into the `out`
directory)

Add the `out/bin` directory to your runtime PATH environment variable so you
can execute the **MiniAT** `mash` assembler and other systems regardless of
the directory you're in.  The following command could also be added to your
shell starup script, such as `~/.bashrc` or `~/.zshrc`.

	export PATH=$PATH:~/miniat/build/out/bin

For Mac users, you'll also need to set the appropriate dynamic linker library
path variable.

	`export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:~/miniat/build/lib`

# Run Stuff

To run an example system, the Mash assembler, or the **MiniAT** test suite, you
can use commands similar to the following (assuming you're in `~/miniat/build`):

- `miniat_color_console out/share/miniat/color_console_demo.bin`
- `miniat_console out/share/miniat/simple_term.bin` (you'll have to use Ctrl+c to exit)
- `mash your_assembly_file.asm` to assemble your code into a `.bin` file to
run on a **MiniAT** system
- `miniat_tests`

[comment]: # (
	MiniAT is now a part of Coverity's open source scanning. You can
	find it here: https://scan.coverity.com/projects/miniat
)
