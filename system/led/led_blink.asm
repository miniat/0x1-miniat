<<< Blink an LED connected to pin 0 of Port A >>>

.constant	PORTA	0x1B1A

!main
	movi	r1 = 0
!loop
	inv	r1
	stor	[PORTA] = r1
	bra	[!loop]
