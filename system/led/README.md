# MiniAT LED

The **MiniAT** LED is as primitive a system as we can imagine.
The Pin 0 of port A (address 0x1B1A) is connected to an LED that
is on when that pin is high, and off otherwise.  The on/off states
are draw by a temporal history of '@' and '.' characters, respectively.

The "led_blink.asm" program simply toggles that pin.  However, the timing
of the state changes provides insight into the complexity of the overall
system complexity (i.e., the effects of caching and branch delays).
