#include <string.h>
#include <stdlib.h>

#include "miniat/miniat.h"

int main(int argc, char *argv[]) {
	if(argc != 2) {
		puts("Must provide a single bin file argument");
		exit(EXIT_FAILURE);
	}

	miniat *m = miniat_file_new(argv[1], NULL);
	printf("MiniAT LED Actionator.  Bit 0 of Port A [0x1B1A] is an LED.\n");

	for(;;) {
		m_wyde portA;

		miniat_clock(m);

		portA = miniat_pins_get_gpio_port(m, m_gpio_id_A);

		if(portA.bits.bit0 == 0) {
			printf(".");
		}
		else {
			printf("@");
		}
	}
	miniat_free(m);

	return EXIT_SUCCESS;
}
