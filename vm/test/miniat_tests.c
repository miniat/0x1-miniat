
/* This is auto-generated code. Edit at your own peril. */
#include <stdio.h>
#include <stdlib.h>

#include "CuTest.h"

extern void t_exec___branch_delay(CuTest *);
extern void t_error___exec_bus_read(CuTest *);
extern void t_error___exec_bus_write(CuTest *);
extern void t_exec___flush(CuTest *);
extern void t_exec___ints_high_low(CuTest *);
extern void t_error___pins_interface_test(CuTest *);
extern void t_error___exec_store_load(CuTest *);
extern void t_error___exec_timers_time(CuTest *);
extern void t_error___exec_timers_count(CuTest *);
extern void t_error___exec_timers_toggle(CuTest *);
extern void t_error___exec_timers_pwm(CuTest *);
extern void t_error___exec_watchdog(CuTest *);
extern void t_pc___remove_first(CuTest *);
extern void t_pc___add_last(CuTest *);
extern void t_pc___clear(CuTest *);
extern void t_error___port_input_w_cache_script(CuTest *);
extern void t_error___port_input_wo_cache_script(CuTest *);
extern void t_registers___read(CuTest *);
extern void t_registers___write(CuTest *);
extern void t_registers___pc(CuTest *);

int RunAllTests(void)
{
    CuString *output = CuStringNew();
    CuSuite* suite = CuSuiteNew();

    SUITE_ADD_TEST(suite, t_exec___branch_delay);
    SUITE_ADD_TEST(suite, t_error___exec_bus_read);
    SUITE_ADD_TEST(suite, t_error___exec_bus_write);
    SUITE_ADD_TEST(suite, t_exec___flush);
    SUITE_ADD_TEST(suite, t_exec___ints_high_low);
    SUITE_ADD_TEST(suite, t_error___pins_interface_test);
    SUITE_ADD_TEST(suite, t_error___exec_store_load);
    SUITE_ADD_TEST(suite, t_error___exec_timers_time);
    SUITE_ADD_TEST(suite, t_error___exec_timers_count);
    SUITE_ADD_TEST(suite, t_error___exec_timers_toggle);
    SUITE_ADD_TEST(suite, t_error___exec_timers_pwm);
    SUITE_ADD_TEST(suite, t_error___exec_watchdog);
    SUITE_ADD_TEST(suite, t_pc___remove_first);
    SUITE_ADD_TEST(suite, t_pc___add_last);
    SUITE_ADD_TEST(suite, t_pc___clear);
    SUITE_ADD_TEST(suite, t_error___port_input_w_cache_script);
    SUITE_ADD_TEST(suite, t_error___port_input_wo_cache_script);
    SUITE_ADD_TEST(suite, t_registers___read);
    SUITE_ADD_TEST(suite, t_registers___write);
    SUITE_ADD_TEST(suite, t_registers___pc);

    CuSuiteRun(suite);
    CuSuiteSummary(suite, output);
    CuSuiteDetails(suite, output);
    printf("%s\n", output->buffer);
    CuStringDelete(output);
    int failed = suite->failCount;
    CuSuiteDelete(suite);
    return failed;
}

int main(void)
{
    return RunAllTests();
}

